const puppeteer = require('puppeteer');

async function run() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  try {
    await page.goto("http://testing-ground.scraping.pro/login")
    await page.type("input#usr", "admin")
    await page.type("input#pwd", "12345")
    let submitButton = await page.$('input[type=submit]')
		await Promise.all([
			submitButton.click(),
			page.waitForNavigation()
		]);
    const text = await page.$eval('h3.success', e => e.textContent)
		console.log(text)
  }
  catch(e) {
    await page.screenshot({ path: 'screenshot.png' })
      .catch(e => console.log("couldn't take screenshot - " + e))
    console.log(e)
  }

  browser.close();
}

run();
